all: cli.cr rot_bruteforcer.cr
	crystal build cli.cr -o broteforcer --release

install: broteforcer
	install -m755 broteforcer /usr/bin/broteforcer

install_local: broteforcer
	install -m755 broteforcer /usr/local/bin/broteforcer

uninstall: broteforcer
	rm /usr/bin/broteforcer

uninstall_local: broteforcer
	rm /usr/local/bin/broteforcer

clean:
	rm -f broteforcer
