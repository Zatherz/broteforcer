require "./rot_bruteforcer"
require "option_parser"

mode = ROTBruteforcer::BruteforceMode::VerifyAgainstDictionary
dict_file_path = nil
debug = false
numbers = false
rotrange = 1..25
percentage = false

parser = OptionParser.parse! do |parser|
	parser.banner = "Usage: broteforcer [options] ENCODEDTEXT\n"\
			"If ran without -a, the result will also print the score of the output.\n"\
			"It determines how many words in the string were found in the dictionary.\n"\
			"A score of 0 usually means that ENCODEDTEXT is not real ROT-encoded text."
	parser.on "-h",          "--help",              "Show this help" { puts parser; exit }
	parser.on "-n",          "--numbers",           "Shift numbers too" { numbers = true }
	parser.on "-s START",    "--range-start=START", "Beginning of the ROT encodings range (1-25)"  { |start| rotrange = start.to_i..rotrange.end }
	parser.on "-e END",      "--range-end=END",     "End of the ROT encodings range (1-25)"  { |end| rotrange = rotrange.begin..end.to_i }
	parser.on "-d DICTFILE", "--dict=DICTFILE",     "Set the dictionary file" { |path| dict_file_path = path }
	parser.on "-v",          "--verbose",           "Enable debug messages" { debug = true }
	parser.on "-a",          "--all",               "Show all 26 variants instead of the one that contains words in the dictionary" {
		mode = ROTBruteforcer::BruteforceMode::ShowAll
	}
end

if rotrange.end > 25 || rotrange.begin < 1
	STDERR.puts "Range doesn't fit in 1..25: " + rotrange.to_s
	STDERR.puts parser
	exit 1
end

if ARGV.size == 0
	STDERR.puts "No encoded string argument passed"
	STDERR.puts parser
	exit 1
end

bruteforcer = nil
bruteforcer = ROTBruteforcer::Bruteforcer.new(mode: mode, dict_file_path: dict_file_path, debug: debug, numbers: numbers, range: rotrange)
bruteforcer.run(ARGV[0]).not_nil!.each do |result|
	if result.score == 0
		STDERR.puts "It seems like the input string is not real ROT-encoded english text!\n"\
			    "bROTeforcer could not find any dictionary words in any of the encodings between ROT-" +
			    rotrange.begin.to_s + " and ROT-" + rotrange.end.to_s + " of the input string.\n"\
			    "Try running with the -a option to get results from all the encodings between ROT-" + rotrange.begin.to_s + " and ROT-" + rotrange.end.to_s + "."
		exit 1
	end
	puts result.to_s
end
