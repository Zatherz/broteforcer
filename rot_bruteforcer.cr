require "logger"
module ROTBruteforcer
	enum BruteforceMode
		VerifyAgainstDictionary
		ShowAll
	end
	class BruteforceResult
		getter content
		getter rotnum
		getter score
		def initialize(@content : String, @rotnum : Int32, @score : Int32 = -1)
		end
		def to_s
			"ROT-" + @rotnum.to_s + ": " + @content + (@score >= 0 ? (" (score: " + @score.to_s + ")") : "")
		end
	end
	class ROTEncoder
		private def rotshift(char : UInt8, step : Int, basechar : Char, max : Int = 26)
			((char + step - basechar.ord) % max + basechar.ord).chr
		end
		def rot(string : String, step : Int = 13, numbers : Bool = false) : String
			raise "String must be ASCII!" if !string.ascii_only?
			String.build do |str|
				string.each_byte do |c|
					if 'a'.ord <= c <= 'z'.ord
						str << rotshift(char: c, step: step, basechar: 'a')
					elsif 'A'.ord <= c <= 'Z'.ord
						str << rotshift(char: c, step: step, basechar: 'A')
					elsif numbers && '0'.ord < c < '9'.ord
						str << rotshift(char: c, step: step, basechar: '0', max: 10)
					else
						str << c.chr
					end
				end
			end
		end
	end
	class Bruteforcer
		def initialize(@mode : BruteforceMode, dict_file_path : String | Nil = nil, debug : Bool = true, @numbers : Bool = false, range @rot_range : Range = 1..25)
			dict_file_path = dict_file_path ? dict_file_path.not_nil! : "./american-english.dict"
			@words = {} of String => Bool
			File.open(dict_file_path).each_line do |line|
				@words[line.rstrip] = true
			end
			@log = Logger.new STDOUT
			@log.level = debug ? Logger::DEBUG : Logger::INFO
		end
		def run(string : String) : Array(BruteforceResult) | Nil
			encoder = ROTEncoder.new
			results = [] of BruteforceResult
			scores = [] of Int32
			@rot_range.each do |i|
				@log.debug("ROT-" + i.to_s)
				rotted = encoder.rot(string: string, step: i, numbers: @numbers)
				case @mode
				when BruteforceMode::ShowAll
					results << BruteforceResult.new(rotted, @rot_range.end - i + 1)
				when BruteforceMode::VerifyAgainstDictionary
					scores << 0
					current_word = String::Builder.new
					rotted.each_char do |char|
						if 'a'.ord <= char.ord <= 'z'.ord ||
						   'A'.ord <= char.ord <= 'Z'.ord ||
						   char == '\''
							current_word << char
						else
							built_word = current_word.to_s
							if @words[built_word.downcase]? && built_word.size > 0
								scores[scores.size - 1] += 1
							end
							current_word = String::Builder.new
						end
					end
					built_word = current_word.to_s
					if @words[built_word.downcase]? && built_word.size > 0
						scores[scores.size - 1] += 1
					end
					# The above is a heavily optimized version of this code:
					# rotted.scan(/([a-zA-Z']+)/) do |matchdata|
					# 	word = matchdata[0]
					# 	if @words[word.downcase]? && !word.match(/^\s*$/)
					# 		scores[scores.size - 1] += 1
					# 	end
					# end
					results << BruteforceResult.new(rotted, @rot_range.end - i + 1, scores[scores.size - 1])
				end
			end
			case @mode
			when BruteforceMode::ShowAll
				return results
			when BruteforceMode::VerifyAgainstDictionary
				return [results[scores.each_with_index.max[1]]]
			end
		end
	end
end
