# bROTeforcer

Simple ROT bruteforcer.

# Building

## Requirements

* Crystal

    make

# Installing

## Install in /usr/bin

    sudo make install

## Install in /usr/local/bin

    sudo make install_local

## Uninstall from /usr/bin

    sudo make uninstall

## Uninstall from /usr/local/bin

    sudo make uninstall_local

# Usage

Run `./broteforcer -h` for full usage info.

## Basic usage

### Decode a ROT-encoded string of unknown ROT type

    ./broteforcer "Uryyb, jbeyq!"

### Narrow down the ROT type range

    ./broteforcer "Uryyb, jbeyq!" --range-start 8 --range-end 14 
    ./broteforcer "Uryyb, jbeyq!" -s 8 -e 14 

### Enable debug info

    ./broteforcer "Uryyb, jbeyq!" -v

### Specify your own english dictionary file

Format is one word per line.  
NOTE: Prepare the file so that all words are lowercase, otherwise it won't work properly.  
Remove all duplicates for performance (reading in the dictionary is the task that takes the longest time to complete).
    
    ./broteforcer -d /usr/share/dict/british "Uryyb, jbeyq!"

### Show all ROT encoding attempts for the encoded string

    ./broteforcer -a "Uryyb, jbeyq!"

### Show ROT encoding attempts for the encoded string in the specified range

    ./broteforcer "Uryyb, jbeyq!" -a -e 13

### Shift number characters too

    ./broteforcer "Uryyb,16jbeyq!15" -n
